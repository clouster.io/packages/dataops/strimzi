#!/usr/bin/env bash

strimzi_version_file="${1:-cnab/strimzi.version}"
kafka_versions_file="${2:-cnab/kafka.versions}"
porter_yaml="${3:-cnab/porter.yaml.tmpl}"
strimzi_components_file="${3:-cnab/strimzi.components}"

strimzi_version="$(cat "$strimzi_version_file")"
opts=()

images_str=
override_str=
for version in $(cat "$kafka_versions_file"); do
  image_name="kafka-${version//./-}"
  version_slug="${version//./_}"
  opts+=("-u" "/images/$image_name/tag=$strimzi_version-kafka-$version")
  images_str="$images_str
$(cat << EOF
  $image_name:
    description: "Kafka $version Image"
    imageType: "docker"
    repository: "quay.io/strimzi/kafka"
    tag: "$CI_COMMIT_TAG"
EOF
)"
  for component in $(cat "$strimzi_components_file"); do
    override_str="$override_str
$(cat << EOF
        - "$component.v$version_slug.image.repository"
        - '"{{ bundle.images.$image_name.repository }}"'
        - "$component.v$version_slug.image.digest"
        - '"{{ bundle.images.$image_name.digest }}"'
EOF
)"
  done
done

latest_kafka="$(cat "$kafka_versions_file" | tail -n1 | tr . -)"

sed -i "s{##KAFKA_IMAGES##{${images_str//$'\n'/\\n}{g" "$porter_yaml"
sed -i "s/##IMAGES_OVERRIDES##/${override_str//$'\n'/\\n}/g" "$porter_yaml"
sed -i "s{\\\$\\\$LATEST_KAFKA\\\$\\\${$latest_kafka{g" "$porter_yaml"

cserph render \
  -u /version="$CI_COMMIT_TAG" \
  -u /registry="$DOCKER_REPO_PROJECT" \
  -u /images/operator/tag="$strimzi_version" \
  -u /images/jmxtrans/tag="$strimzi_version" \
  -u /images/kaniko-executor/tag="$strimzi_version" \
  -u /images/maven-builder/tag="$strimzi_version" \
  "${opts[@]}"
